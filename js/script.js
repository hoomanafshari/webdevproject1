//Hooman Afshari 2134814
window.addEventListener('DOMContentLoaded', function () {
    const projects = new Array();
    const resultArr = new Array(8);
    var submitbutton = document.getElementById("submit");
    formValidate();
    document.getElementById("reset").addEventListener('click', resetForm);
    submitbutton.addEventListener('click', addToObject);
    document.getElementById("queryBox").addEventListener("keydown", searchInTable);
    document.getElementById("write").addEventListener("click", writeToLocalStorage);
    document.getElementById("append").addEventListener("click", appendToLocalStorage);
    this.document.getElementById("load").addEventListener("click", loadFromLocalStorage);
    document.getElementById("clear").addEventListener("click", clear)

    function clear() {
        localStorage.clear();
    }
    function loadFromLocalStorage() {
        const newProjects = new Array();
        //retrieve old projects from local storage  
        for (var i = 0; i < localStorage.length; i++) {
            newProjects[i] = JSON.parse(localStorage.getItem(i));
            projects.push(newProjects[i]);
            addToTable();
        }

    }
    function appendToLocalStorage() {
        const oldProjects = new Array();
        //retrieve old projects from local storage  
        for (var i = 0; i < projects.length; i++) {
            for (var j = 0; j < localStorage.length; j++) {
                oldProjects[j] = JSON.parse(localStorage.getItem(j));
                if (oldProjects[j] !== projects[i]) {
                    oldProjects.push(projects[i]);
                }
            }
        }
        //append the updated array into the local storage
        for (var i = 0; i < oldProjects.length; i++) {
            localStorage.setItem(i, JSON.stringify(oldProjects[i]));
        }
    }

    function writeToLocalStorage() {
        clear();
        for (var i = 0; i < projects.length; i++) {
            localStorage.setItem(i, JSON.stringify(projects[i]));
        }
    }
    function searchInTable() {
        //gets input from the search bar
        let filter = this.value.toUpperCase();
        matchNum = 0;
        //Arrow function,checks if input matches any field and it will display matches
        let checkMatch = () => {
            //loops over all the projects
            for (var i = 0; i < projects.length; i++) {
                var id = projects[i].projectId;
                var owner = projects[i].ownName;
                var title = projects[i].title;
                var category = projects[i].category;
                var status = projects[i].status;
                var hours = projects[i].hours;
                var rate = projects[i].rate;
                var desc = projects[i].desc;
                //checks if the input matches any row
                if (id.toUpperCase().indexOf(filter) > -1 || title.toUpperCase().indexOf(filter) > -1 ||
                    owner.toUpperCase().indexOf(filter) > -1 || category.toUpperCase().indexOf(filter) > -1 ||
                    status.toUpperCase().indexOf(filter) > -1 || hours.toUpperCase().indexOf(filter) > -1 ||
                    rate.toUpperCase().indexOf(filter) > -1 || desc.toUpperCase().indexOf(filter) > -1) {
                    document.getElementById(i).style.display = "";
                    matchNum++;
                }
                //removes the row that does not match
                else {
                    document.getElementById(i).style.display = "none";
                }
            }
            return matchNum;
        }
        //prints the number of matches in the search bar
        var tableInfo = document.getElementById("tableInfo").childNodes[1];
        tableInfo.innerHTML = "<p> Your query returned " + checkMatch() + " projects in the table <p>";
    }

    function addToObject() {
        //checks if progect id is unique
        var projectId = document.getElementById("projectId").value;
        for (var i = 0; i < projects.length; i++) {
            if (projects[i] != undefined) {
                if (projectId == projects[i].projectId) {
                    alert("Your project id should be unique, your project was not added")
                    return -1;
                }
            }
        }
        //creates a project object
        const project = {
            projectId: document.getElementById("projectId").value,
            ownName: document.getElementById("ownName").value,
            title: document.getElementById("title").value,
            category: document.getElementById("category").value,
            hours: document.getElementById("hours").value,
            rate: document.getElementById("rate").value,
            status: document.getElementById("status").value,
            desc: document.getElementById("desc").value
        }
        //push it to the project array
        projects.push(project);
        addToTable();
    }
    //sorts the projects
    function sortProjects() {
        projects.sort(function (a, b) {
            const first = a.projectId.toUpperCase();
            const second = b.projectId.toUpperCase();
            //checks if second should come before first
            if (first < second) {
                return -1;
            }
            //checks if first should come before second
            if (first > second) {
                return 1;
            }
            return 0;
        });
    }
    //Creates the table
    function addToTable() {
        sortProjects();
        var htmlTable = '';
        for (var i = 0; i < projects.length; i++) {
            htmlTable += "<tr id=\"" + i + "\" >";
            htmlTable += "<td>" + projects[i].projectId + "</td>";
            htmlTable += "<td>" + projects[i].ownName + "</td>";
            htmlTable += "<td>" + projects[i].title + "</td>";
            htmlTable += "<td>" + projects[i].category + "</td>";
            htmlTable += "<td>" + projects[i].hours + "</td>";
            htmlTable += "<td>" + projects[i].rate + "</td>";
            htmlTable += "<td>" + projects[i].status + "</td>";
            htmlTable += "<td>" + projects[i].desc + "</td>";
            htmlTable += '<td class=\"edit\"><img src=\"../image/edit.png\"></td>';
            htmlTable += '<td class=\"garbage\"><img src=\"../image/garbage.jpg\"></td>';
            htmlTable += "</tr>";
        }
        document.getElementById("tbody").innerHTML = htmlTable;
        //display information about the table
        var tableInfo = document.getElementById("tableInfo").childNodes[1];
        tableInfo.innerHTML = "<p> You have " + projects.length + " project(s) in the table <p>";
        //gets all edit icons in the table
        var editTds = document.querySelectorAll(".edit");
        //checks if user clicks on edit in any row
        editTds.forEach(td => {
            td.addEventListener('click', editTd);
        })
        //gets all garbage icons in the table
        var editTds = document.querySelectorAll(".garbage");
        //checks if user clicks on garbage in any row
        editTds.forEach(td => {
            td.addEventListener('click', removeProject);
        })
        function removeProject(e) {
            //finds the specific row that user wants to remove
            var target = e.target
            var td = target.parentElement;
            var tr = td.parentElement;
            //gets confirmation from the user
            if (confirm("Press ok if you want to delete the project") == true) {
                projects.splice(tr.id, 1);
                //updates the table
                addToTable();
            }
        }
    }
    function editTd(e) {
        //finds the specific row that user wants to edit
        var target = e.target
        var td = target.parentElement;
        var tr = td.parentElement;
        //makes columns input and changes the edit icon
        var htmlInput = "";
        htmlInput += "<td><input type=\"text\" id=\"newProjId\"</td>";
        htmlInput += "<td><input type=\"text\" id=\"newOwner\"</td>";
        htmlInput += "<td><input type=\"text\" id=\"newTitle\"</td>";
        htmlInput += "<td><input type=\"text\" id=\"newCategory\"</td>";
        htmlInput += "<td><input type=\"text\" id=\"newHours\"</td>";
        htmlInput += "<td><input type=\"text\" id=\"newRate\"</td>";
        htmlInput += "<td><input type=\"text\" id=\"newStatus\"</td>";
        htmlInput += "<td><input type=\"text\" id=\"newDesc\"</td>";
        htmlInput += '<td class=\"save\"><img src=\"../image/save.jpg\"></td>';
        htmlInput += '<td class=\"garbage\"><img src=\"../image/garbage.jpg\"></td>';
        tr.innerHTML = htmlInput;
        //changes the save icon and updates the data in the array and table
        var saveTds = document.querySelectorAll(".save");
        saveTds.forEach(td => {
            td.addEventListener('click', function () {
                projects[tr.id].projectId = document.getElementById("newProjId").value;
                projects[tr.id].ownName = document.getElementById("newOwner").value;
                projects[tr.id].title = document.getElementById("newTitle").value;
                projects[tr.id].category = document.getElementById("newCategory").value;
                projects[tr.id].hours = document.getElementById("newHours").value;
                projects[tr.id].rate = document.getElementById("newRate").value;
                projects[tr.id].status = document.getElementById("newStatus").value;
                projects[tr.id].desc = document.getElementById("newDesc").value;
                addToTable();
            });
        });
    }
    function resetForm() {
        var wrongAlerts = document.getElementsByClassName("wrong");
        if (wrongAlerts != "") {
            for (var i = 0; i < wrongAlerts.length; ++i) {
                wrongAlerts[i].style.display = "none";
            }
        }
        var crossImages = document.getElementsByClassName("cross");
        var checkImages = document.getElementsByClassName("check");

        if (crossImages != "") {
            for (var i = 0; i < crossImages.length; ++i) {
                crossImages[i].style.display = "none";
            }
        }
        if (checkImages != "") {
            for (var i = 0; i < checkImages.length; i++) {
                checkImages[i].style.display = "none";
            }
        }
        for (var i = 0; i < resultArr.length; i++) {
            resultArr[i] = false;
        }
        submitbutton.style.backgroundColor = "grey";
        submitbutton.setAttribute("disabled", "");
    }
    function removeWrongAlerts(e) {
        var wrongAlerts = document.getElementById('invalid' + e + '');
        if (wrongAlerts == undefined) {
            return -1;
        }
        wrongAlerts.style.display = "none";
    }

    function formValidate() {

        var projId = document.getElementById("projectId");
        var ownName = document.getElementById("ownName");
        var title = document.getElementById("title");
        var category = document.getElementById("category");
        var hours = document.getElementById("hours");
        var rate = document.getElementById("rate");
        var status = document.getElementById("status");
        var desc = document.getElementById("desc");

        projId.addEventListener('input', checkInput);
        ownName.addEventListener('input', checkInput);
        title.addEventListener('input', checkInput);
        category.addEventListener('input', checkInput);
        hours.addEventListener('input', checkInput);
        rate.addEventListener('input', checkInput);
        status.addEventListener('input', checkInput);
        desc.addEventListener('input', checkInput);

        function checkInput(e) {
            const alphanum = /^[a-z0-9_ ]+$/i;
            const numeric = /^[0-9]+$/;
            const alpha = /^[a-z_ ]+$/i;
            var status;
            if (e.target.id == "status" && this.value != "---") {
                displayCheck(e.target.id);
            }
            else if (alphanum.test(this.value) && (e.target.id == "projectId" || e.target.id == "title" || e.target.id == "desc") && this.value != " ") {
                displayCheck(e.target.id);
            }
            else if (numeric.test(this.value) && (e.target.id == "hours" || e.target.id == "rate" || e.target.id == "status")) {
                displayCheck(e.target.id);
            }
            else if (alpha.test(this.value) && (e.target.id == "ownName" || e.target.id == "category" || e.target.id == "status") && this.value != " ") {
                displayCheck(e.target.id);
            }
            else {
                displayCross(e.target.id);
            }
        }
        function displayCheck(e) {
            var imgTag = document.createElement("img");
            imgTag.setAttribute("src", "../image/check.png");
            imgTag.setAttribute("class", "check");
            switch (e) {
                case "projectId":
                    document.getElementById("firstField").replaceChild(imgTag, document.getElementById("firstField").childNodes[6]);
                    removeWrongAlerts(e);
                    resultArr[0] = true;
                    break;
                case "ownName":
                    document.getElementById("firstField").replaceChild(imgTag, document.getElementById("firstField").childNodes[12]);
                    removeWrongAlerts(e);
                    resultArr[1] = true;
                    break;
                case "title":
                    document.getElementById("secondField").replaceChild(imgTag, document.getElementById("secondField").childNodes[4]);
                    removeWrongAlerts(e);
                    resultArr[2] = true;
                    break;
                case "category":
                    document.getElementById("secondField").replaceChild(imgTag, document.getElementById("secondField").childNodes[10]);
                    removeWrongAlerts(e);
                    resultArr[3] = true;
                    break;
                case "hours":
                    document.getElementById("thirdField").replaceChild(imgTag, document.getElementById("thirdField").childNodes[4]);
                    removeWrongAlerts(e);
                    resultArr[4] = true;
                    break;
                case "rate":
                    document.getElementById("thirdField").replaceChild(imgTag, document.getElementById("thirdField").childNodes[11]);
                    removeWrongAlerts(e);
                    resultArr[5] = true;
                    break;
                case "status":
                    document.getElementById("thirdField").replaceChild(imgTag, document.getElementById("thirdField").childNodes[16]);
                    removeWrongAlerts(e);
                    resultArr[6] = true;
                    break;
                case "desc":
                    document.getElementById("forthField").replaceChild(imgTag, document.getElementById("forthField").childNodes[5]);
                    removeWrongAlerts(e);
                    resultArr[7] = true;
            }
            if (resultArr[0] == true && resultArr[1] == true && resultArr[2] == true && resultArr[3] == true && resultArr[4] == true && resultArr[5] == true && resultArr[6] == true && resultArr[7] == true) {
                submitbutton.style.backgroundColor = "dodgerblue";
                submitbutton.removeAttribute("disabled");
            }
        }
        function displayCross(e) {
            var newDiv = document.createElement("div");
            newDiv.setAttribute("class", "wrong");
            newDiv.setAttribute("id", "invalid" + e + "");
            var text = document.createTextNode("You have wrong input for " + e);
            newDiv.appendChild(text);

            var imgTag = document.createElement("img");
            imgTag.setAttribute("src", "../image/cross.jpg");
            imgTag.setAttribute("class", "cross");

            switch (e) {
                case "ownName":
                    document.getElementById("firstField").replaceChild(imgTag, document.getElementById("firstField").childNodes[12]);
                    document.getElementById("firstField").replaceChild(newDiv, document.getElementById("firstField").childNodes[13]);
                    resultArr[1] = false;
                    break;
                case "projectId":
                    document.getElementById("firstField").replaceChild(imgTag, document.getElementById("firstField").childNodes[6]);
                    document.getElementById("firstField").replaceChild(newDiv, document.getElementById("firstField").childNodes[7]);
                    resultArr[0] = false;
                    break;
                case "title":
                    document.getElementById("secondField").replaceChild(imgTag, document.getElementById("secondField").childNodes[4]);
                    document.getElementById("secondField").replaceChild(newDiv, document.getElementById("secondField").childNodes[5]);
                    resultArr[2] = false;
                    break;
                case "hours":
                    document.getElementById("thirdField").replaceChild(imgTag, document.getElementById("thirdField").childNodes[4]);
                    document.getElementById("thirdField").replaceChild(newDiv, document.getElementById("thirdField").childNodes[5]);
                    resultArr[4] = false;
                    break;
                case "rate":
                    document.getElementById("thirdField").replaceChild(imgTag, document.getElementById("thirdField").childNodes[11]);
                    document.getElementById("thirdField").replaceChild(newDiv, document.getElementById("thirdField").childNodes[12]);
                    resultArr[5] = false;
                    break;
                case "category":
                    document.getElementById("secondField").replaceChild(imgTag, document.getElementById("secondField").childNodes[10]);
                    document.getElementById("secondField").replaceChild(newDiv, document.getElementById("secondField").childNodes[11]);
                    resultArr[3] = false;
                    break;
                case "status":
                    document.getElementById("thirdField").replaceChild(imgTag, document.getElementById("thirdField").childNodes[16]);
                    document.getElementById("thirdField").replaceChild(newDiv, document.getElementById("thirdField").childNodes[17]);
                    resultArr[6] = false;
                    break;
                case "desc":
                    document.getElementById("forthField").replaceChild(imgTag, document.getElementById("forthField").childNodes[5]);
                    document.getElementById("forthField").replaceChild(newDiv, document.getElementById("forthField").childNodes[6]);
                    resultArr[7] = false;
            }
            if (resultArr[0] == false || resultArr[1] == false || resultArr[2] == false || resultArr[3] == false || resultArr[4] == false || resultArr[5] == false || resultArr[6] == false || resultArr[7] == false) {
                submitbutton.style.backgroundColor = "grey";
                submitbutton.setAttribute("disabled", "");
            }
        }
    }
});
